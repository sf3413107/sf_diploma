from diagrams import Cluster, Diagram, Edge
from diagrams.aws.network import ELB
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.logging import Fluentbit, Loki
from diagrams.onprem.database import Postgresql
from diagrams.generic.storage import Storage
from diagrams.k8s.network import SVC
from diagrams.k8s.storage import PVC
from diagrams.alibabacloud.application import LogService
from diagrams.onprem.network import Nginx
from diagrams.onprem.client import Client
from diagrams.gcp.analytics import Dataflow
from diagrams.onprem.ci import GitlabCI
from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS
from diagrams.aws.database import ElastiCache, RDS
from diagrams.aws.network import Route53
from diagrams.elastic.elasticsearch import ElasticSearch


with Diagram("SkillFactory Diploma", show=False):
    gitlab = GitlabCI("gitlab.com")
    with Cluster("Yandex Cloud"):
        
        with Cluster("Monitoring"):
            primary = Prometheus("Prometheus")
            secondary = Grafana("Grafana")
            primary - secondary
            tertiary = Loki("Loki")
            GitlabCI("Gitlab Runner") << gitlab 

        with Cluster("K8s cluster"):
            Loki("Promtail") >> tertiary 
            PVC("Storage") - Postgresql("DB") - SVC("Service") - ELB("load balancer") << primary
        
    