##### Kubernetes cluster #####

resource "yandex_kubernetes_cluster" "k8s-zonal" {
  network_id = yandex_vpc_network.mynet.id
  master {
    version = local.local_data.k8s_version
    zonal {
      zone      = yandex_vpc_subnet.mysubnet.zone
      subnet_id = yandex_vpc_subnet.mysubnet.id
    }
    public_ip = true
  }
  service_account_id      = local.local_data.sa_id
  node_service_account_id = local.local_data.sa_id
}
resource "yandex_vpc_network" "mynet" {
  name = "mynet"
}
resource "yandex_vpc_subnet" "mysubnet" {
  v4_cidr_blocks = ["10.16.0.0/16"]
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.mynet.id
}

##### Add nodes to k8s cluster #####

resource "yandex_kubernetes_node_group" "node-group-0" {
  cluster_id = yandex_kubernetes_cluster.k8s-zonal.id
  name       = "node-group-0"
  version    = "1.22"
  instance_template {
    platform_id = "standard-v2"
    resources {
      memory        = 8
      cores         = 4
      core_fraction = 50
    }
    boot_disk {
      type = "network-hdd"
      size = 32
    }
    scheduling_policy {
      preemptible = true
    }
    network_interface {
      subnet_ids = [yandex_vpc_subnet.mysubnet.id]
      nat        = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
  }
  scale_policy {
    fixed_scale {
      size = 1
    }
  }
  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }
  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true
    maintenance_window {
      day        = "monday"
      start_time = "2:00"
      duration   = "3h"
    }
    maintenance_window {
      day        = "thursday"
      start_time = "2:00"
      duration   = "3h"
    }
  }
}

##### Kube config #####

locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${yandex_kubernetes_cluster.k8s-zonal.master[0].external_v4_endpoint}
    certificate-authority-data: ${base64encode(yandex_kubernetes_cluster.k8s-zonal.master[0].cluster_ca_certificate)}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: yc
  name: ycmk8s
current-context: ycmk8s
users:
- name: yc
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: yc
      args:
      - k8s
      - create-token
KUBECONFIG
}

##### Create kube config files #####

resource "local_file" "kube_config" {
  content              = local.kubeconfig
  filename             = "/home/petr/.kube/config"
  file_permission      = "0600"
  directory_permission = "0755"
}

resource "local_file" "kube_config2" {
  content              = local.kubeconfig
  filename             = "${path.module}/../src/kube_config"
  file_permission      = "0600"
  directory_permission = "0755"
}

##### IP address #####

resource "yandex_vpc_address" "addr" {
  name = "static-ip"
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_dns_recordset" "lb_name" {
  depends_on = [yandex_vpc_address.addr]
  zone_id    = local.local_data.zone_id
  name       = "lb"
  type       = "A"
  ttl        = 200
  data       = [yandex_vpc_address.addr.external_ipv4_address[0].address]
}


##### Helm /src/deploy/kubernetes/app-chart/values.yaml #####

resource "local_file" "values" {  
  content  = <<EOF

replicaCount: 1

image:
  repository: petrakimovdocker/skillfactory
  tag: latest
  pullPolicy: Always

lb:
  name: django-lb
  port: 8000
  targetPort: 8000
  ipAddress: "${yandex_vpc_address.addr.external_ipv4_address[0].address}"

dbReplicaCount: 1

dbImage:
  repository: postgres
  tag: 9.6
  pullPolicy: Always

dbSecret:
  user: cG9zdGdyZXM=
  password: MTIzNDU2

EOF 
  filename = "${path.module}/../src/deploy/kubernetes/app-chart/values.yaml"
}

