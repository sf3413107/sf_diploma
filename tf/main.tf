##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
  #    backend "http" {}
}

#variable "YANDEX_OAUTH" {
#  type = string
#}

provider "yandex" {
  #  token = var.YANDEX_OAUTH
  #  token                    = file(local.local_data.OAUTH_FILE)  
  service_account_key_file = local.local_data.KEY_FILE
  cloud_id                 = local.local_data.CLOUD_ID
  folder_id                = local.local_data.FOLDER_ID
  zone                     = local.local_data.ZONE
}

