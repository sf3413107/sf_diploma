##### VMs Part #####

resource "yandex_compute_instance" "monitor" {
  name        = "monitor"
  platform_id = "standard-v2"
  resources {
    core_fraction = 50
    cores         = "4"
    memory        = "4"
  }
  boot_disk {
    initialize_params {
      image_id = "fd81d2d9ifd50gmvc03g" # ubuntu-18
      #      image_id = "fd8j9i69vt27ujq6rqug" # centos-8
      #      image_id = "fd87uuu91tfgdpimc7re" # grafana-webserver
      #      image_id = "fd8gnpl76tcrdv0qsfko" # ubuntu-20
      #      image_id = "fd8b24tqvq7t2f8a1o1s" # ubuntu-22
      size = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.mysubnet.id
    nat       = true
  }
  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
  scheduling_policy {
    preemptible = true
  }
}

##### Create file inventory #####

resource "local_file" "inventory" {
  content  = <<EOF

[monitor]
${yandex_compute_instance.monitor.network_interface.0.nat_ip_address} ansible_ssh_extra_args='-o StrictHostKeyChecking=no'

EOF
  filename = "${path.module}/inventory"
}

##### Provisioning #####

resource "null_resource" "monitor" {
  depends_on = [yandex_compute_instance.monitor, local_file.inventory]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.monitor.network_interface.0.nat_ip_address
  }
  provisioner "file" {
    source      = "${path.module}/test"
    destination = "/home/ubuntu/test"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i inventory --key-file ~/.ssh/id_rsa ../ansible/monitoring.yml"
  }
}

##### Add A-record to DNS zone #####

resource "yandex_dns_recordset" "app_dns_name" {
  depends_on = [yandex_compute_instance.monitor, local_file.inventory]
  zone_id    = local.local_data.zone_id
  name       = local.local_data.app_dns_name
  type       = "A"
  ttl        = 200
  data       = [yandex_compute_instance.monitor.network_interface.0.nat_ip_address]
}
