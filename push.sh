version=5.4.22
git tag | xargs git tag -d
git add .
git commit --allow-empty -m "ver $version"
git tag $version
git push -uf origin main --tags
